export interface ISpriteConstructor {
    _scene: Phaser.Scene
    _posX: number
    _posY: number
    _texture: string
    _frame?: string | number
}
