const createAnimations = (_context, _key, _nameTexture, _framesConfig, _frameRate, _repeat) => {
    _context.anims.create({
        key: _key,
        frames: _context.anims.generateFrameNumbers(_nameTexture, _framesConfig),
        frameRate: _frameRate,
        repeat: _repeat
    })
}
export default createAnimations