import Phaser from 'phaser'
import { Office } from './office'

const GameConfig: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    backgroundColor: 'black',
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        parent: 'office',
        width: 1080,
        height: 800
          },
    scene: Office,
    render:{
        pixelArt: true
    },
    physics: {
        default: 'arcade',
        arcade: {
          gravity: {y: 0},
          debug: true,
          debugShowVelocity: true,
          debugShowBody: true,
          debugShowStaticBody: true
        }
      }
}

export default GameConfig