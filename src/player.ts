import { ISpriteConstructor } from './interfaces/sprite.interface'
import createAnimations from './utils/createAnimation'

export class Player extends Phaser.GameObjects.Sprite {
    body: Phaser.Physics.Arcade.Body

    public scene: Phaser.Scene

    private _cursor: Phaser.Types.Input.Keyboard.CursorKeys

    constructor(params : ISpriteConstructor){
        super(params._scene, params._posX, params._posY, params._texture, params._frame) 

        this.scene = params._scene

        this.scene.add.existing(this)
        this.scene.physics.add.existing(this)

        /** animations manager */
        createAnimations(this, 'adam_idle', 'Adam_idle', {start:18, end:23}, 6, -1)
        createAnimations(this, 'adam_run_right', 'Adam_run', {start:0, end:5}, 6, -1)
        createAnimations(this, 'adam_run_left', 'Adam_run', {start:12, end:17}, 6, -1)
        createAnimations(this, 'adam_run_up', 'Adam_run', {start:6, end:11}, 6, -1)
        createAnimations(this, 'adam_run_down', 'Adam_run', {start:18, end:23}, 6, -1)

        /** Properties */
        this.body.setCollideWorldBounds(true)

        this._cursor = this.scene.input.keyboard.createCursorKeys()
    }

    preUpdate(time, delta){
        super.preUpdate(time, delta)
        if (this._cursor.right.isDown) 
        {
            this.body.setVelocityX(40)
            this.anims.play('adam_run_right', true)
        }
        else if (this._cursor.left.isDown) 
        {
            this.body.setVelocityX(-40)
            this.anims.play('adam_run_left', true)
        }
        else if(this._cursor.down.isDown)
        {
            this.body.setVelocityY(40)
            this.anims.play('adam_run_down', true)
        }
        else if(this._cursor.up.isDown)
        {
            this.body.setVelocityY(-40)
            this.anims.play('adam_run_up', true)
        }
        else
        {
            this.body.setVelocity(0)
            this.anims.play('adam_idle', true)
        }
    }

}