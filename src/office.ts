import { Scene } from 'phaser'
import { Player } from './player'

/**
 * Main scene
 */
export class Office extends Scene {

/**Tilemap Layers*/
/** @type {Phaser.Tilemaps.Tilemap} */
public tilemap: Phaser.Tilemaps.Tilemap

/** @type {Phaser.Tilemaps.Tileset} */
private _tileset: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetLivingRoom: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetClassRoom: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetMusic: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetGym: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetKitchen: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetBasement: Phaser.Tilemaps.Tileset
/** @type {Phaser.Tilemaps.Tileset} */
private _tilesetJail: Phaser.Tilemaps.Tileset

/** @type {Phaser.Tilemaps.TilemapLayer} */
private _wallLayer: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _background: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _ground: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _tapestry: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _kitchen: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _kitchen2: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _pingpong: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _pingpong2: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _office: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _office2: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _officer: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _gaming: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _couchs: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _decoration: Phaser.Tilemaps.TilemapLayer
/** @type {Phaser.Tilemaps.TilemapLayer} */
private _gaming2: Phaser.Tilemaps.TilemapLayer

/** @type {Player} */
protected player: Player

    constructor (){
        super({key: 'Office'})
    }

    preload(): void {
        this.load.image('tileset', './assets/images/Old_stuff/Tileset_16x16_13.png')
        this.load.image('tilesetLivingRoom', './assets/images/Theme_Sorter/2_LivingRoom_16x16.png')
        this.load.image('tilesetClassRoom', './assets/images/Theme_Sorter/5_Classroom_and_library_16x16.png')
        this.load.image('tilesetMusic', './assets/images/Theme_Sorter/6_Music_and_sport_16x16.png')
        this.load.image('tilesetGym', './assets/images/Theme_Sorter/8_Gym_16x16.png')
        this.load.image('tilesetKitchen', './assets/images/Theme_Sorter/12_Kitchen_16x16.png')
        this.load.image('tilesetBasement', './assets/images/Theme_Sorter/14_Basement_16x16.png')
        this.load.image('tilesetJail', './assets/images/Theme_Sorter/18_Jail_16x16.png')

        this.load.tilemapTiledJSON('map', './assets/json/office_simulator.json')

        this.load.spritesheet('Adam_idle', './assets/images/Characters/Adam/Adam_idle_anim_16x16.png', { frameWidth: 16, frameHeight: 32 })
        this.load.spritesheet('Adam_run', './assets/images/Characters/Adam/Adam_run_16x16.png', { frameWidth: 16, frameHeight: 32 })
    }

    create(): void {
        this.tilemap = this.make.tilemap({ key: 'map' })
        this._tileset = this.tilemap.addTilesetImage('tileset', 'tileset', 16, 16)
        this._tilesetBasement = this.tilemap.addTilesetImage('basement', 'tilesetBasement', 16, 16)
        this._tilesetClassRoom = this.tilemap.addTilesetImage('classroom', 'tilesetClassRoom', 16, 16)
        this._tilesetGym = this.tilemap.addTilesetImage('gym', 'tilesetGym', 16, 16)
        this._tilesetJail = this.tilemap.addTilesetImage('jail', 'tilesetJail', 16, 16)
        this._tilesetKitchen = this.tilemap.addTilesetImage('kitchen', 'tilesetKitchen', 16, 16)
        this._tilesetLivingRoom = this.tilemap.addTilesetImage('livingRoom', 'tilesetLivingRoom', 16, 16)
        this._tilesetMusic = this.tilemap.addTilesetImage('music', 'tilesetMusic', 16, 16)

        this._background = this.tilemap.createLayer('Background', this._tileset, 0, 0)
        this._ground = this.tilemap.createLayer('Ground', this._tileset, 0, 0)
        this._tapestry = this.tilemap.createLayer('Tapestry', [this._tilesetBasement, this._tilesetKitchen], 0, 0)
        this._wallLayer = this.tilemap.createLayer('Wall', this._tileset, 0, 0)
        this._kitchen = this.tilemap.createLayer('Kitchen', this._tilesetKitchen, 0, 0)
        this._kitchen2 = this.tilemap.createLayer('Kitchen2', this._tilesetKitchen, 0, 0)
        this._pingpong = this.tilemap.createLayer('PingPong', [this._tilesetBasement, this._tilesetLivingRoom], 0, 0)
        this._pingpong2 = this.tilemap.createLayer('PingPong2', [this._tilesetBasement, this._tilesetLivingRoom], 0, 0)
        this._office = this.tilemap.createLayer('Office', this._tilesetBasement, 0, 0)
        this._office2 = this.tilemap.createLayer('Office2', [this._tilesetBasement, this._tilesetJail], 0, 0)
        this._officer = this.tilemap.createLayer('Officer', [this._tilesetBasement, this._tilesetJail, this._tilesetClassRoom], 0, 0)
        this._gaming = this.tilemap.createLayer('Gaming', [this._tilesetBasement, this._tilesetGym], 0, 0)
        this._couchs = this.tilemap.createLayer('Couchs', [this._tilesetBasement, this._tilesetLivingRoom, this._tilesetMusic], 0, 0)
        this._decoration = this.tilemap.createLayer('Decoration', [this._tilesetBasement, this._tilesetLivingRoom], 0, 0)
        this._gaming2 = this.tilemap.createLayer('Gaming2', [this._tilesetBasement, this._tilesetLivingRoom, this._tilesetGym], 0, 0)

        this._wallLayer.setCollisionByProperty({collide : true})

        this.player = new Player({_scene:this,_posX:350, _posY:200, _texture:'Adam_idle', _frame:3}) 
        this.physics.add.collider(this.player, this._wallLayer, ()=>console.log('test collison'))

        this.cameras.main.setBounds(0, 0, 640, 640)
        this.cameras.main.startFollow(this.player, true, 0.09, 0.09)
        this.cameras.main.setZoom(4)
    }

}
