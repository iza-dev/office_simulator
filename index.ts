import Server from './server'

const PORT = 5000 || 8000

const server = new Server(PORT)
server.start()