import express, {Request, Response} from 'express'

export default class Server{
  constructor(private port:number){ }
  public start(): void{
    const app = express()
    app
    .use(express.static(__dirname + '/public'))

    .get('/', (req:Request, res:Response) => {
      res.sendFile(__dirname + '/index.html')
    })

    .listen(this.port, () => {
      console.log(`server ok on ${this.port}`)
    })
  }
}